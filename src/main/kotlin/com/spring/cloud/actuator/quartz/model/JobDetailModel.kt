package com.spring.cloud.actuator.quartz.model

class JobDetailModel(
    name: String,
    isConcurrentDisallowed: Boolean,
    isDurable: Boolean,
    jobClass: String,
    val group: String,
    val triggers: List<TriggerDetailModel>,
) : JobModel(name, isConcurrentDisallowed, isDurable, jobClass)
