package com.spring.cloud.actuator.quartz.exception

class UnsupportedStateChangeException(message: String) : RuntimeException(message)
