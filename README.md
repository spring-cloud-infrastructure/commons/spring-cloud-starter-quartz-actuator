# Spring Cloud Starter QUARTZ Actuator

Provides Spring auto-configuration for [QUARTZ](http://www.quartz-scheduler.org/) actuator endpoints, inspired
by [quartz-actuator](https://github.com/sathyabodh/quartz-acutator).

## 1. Install to local Maven repository

```shell
./gradlew clean publishToMavenLocal
```

## 2. Use in a project

- Add dependency:

```kotlin
implementation("com.spring.cloud:spring-cloud-starter-quartz-actuator:<VERSION>")
```

## 3. Endpoints

### Jobs

- `GET /actuator/quartz-jobs` - List jobs
- `GET /actuator/quartz-jobs/{group}/{name}` - Get job detail
- `POST /actuator/quartz-jobs/{group}/{name}/{state}` - Change job state (`state` = `PAUSE`/`RESUME`)
- `POST /actuator/quartz-jobs/{group}/{state}` - Change state for entire jobs group (`state` = `PAUSE`/`RESUME`)

### Triggers

- `GET /actuator/quartz-triggers` - List triggers
- `POST /actuator/quartz-triggers/{group}/{name}/{state}` - Change trigger state (`state` = `PAUSE`/`RESUME`)
- `POST /actuator/quartz-triggers/{group}/{state}` - Change state for entire triggers group (`state` = `PAUSE`
  /`RESUME`)
