package com.spring.cloud.actuator.quartz.endpoint

import com.spring.cloud.actuator.quartz.exception.UnsupportedStateChangeException
import com.spring.cloud.actuator.quartz.model.QuartzState
import mu.KotlinLogging.logger
import org.springframework.boot.actuate.endpoint.annotation.Selector
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation
import org.springframework.boot.actuate.endpoint.web.WebEndpointResponse
import org.springframework.boot.actuate.endpoint.web.WebEndpointResponse.STATUS_BAD_REQUEST
import org.springframework.boot.actuate.endpoint.web.WebEndpointResponse.STATUS_NOT_FOUND
import org.springframework.boot.actuate.endpoint.web.WebEndpointResponse.STATUS_OK
import org.springframework.boot.actuate.endpoint.web.annotation.EndpointWebExtension

@EndpointWebExtension(endpoint = QuartzJobEndPoint::class)
class QuartzJobEndPointWebExtension(private val quartzJobEndPoint: QuartzJobEndPoint) {

    private val logger = logger { }

    @WriteOperation
    fun modifyJobState(
        @Selector group: String,
        @Selector name: String,
        @Selector state: QuartzState,
    ): WebEndpointResponse<Any> {
        return try {
            quartzJobEndPoint.modifyJobState(group, name, state).let(::buildWebEndpointResponse)
        } catch (e: UnsupportedStateChangeException) {
            logger.warn(e) { "Unsupported state change: [$state] on job: [$group.$name]" }
            WebEndpointResponse<Any>(STATUS_BAD_REQUEST)
        }
    }

    @WriteOperation
    fun modifyJobsState(
        @Selector group: String,
        @Selector state: QuartzState,
    ): WebEndpointResponse<Any> {
        return try {
            quartzJobEndPoint.modifyJobsState(group, state).let(::buildWebEndpointResponse)
        } catch (e: UnsupportedStateChangeException) {
            logger.warn(e) { "Unsupported state change: [$state] on job group: [$group]" }
            WebEndpointResponse<Any>(STATUS_BAD_REQUEST)
        }
    }

    private fun buildWebEndpointResponse(isSuccess: Boolean): WebEndpointResponse<Any> {
        val status = when {
            isSuccess -> STATUS_OK
            else -> STATUS_NOT_FOUND
        }

        return WebEndpointResponse<Any>(status)
    }
}
