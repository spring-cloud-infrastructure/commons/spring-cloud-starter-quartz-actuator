package com.spring.cloud.actuator.quartz.model

enum class QuartzState {

    PAUSE,
    RESUME
}
