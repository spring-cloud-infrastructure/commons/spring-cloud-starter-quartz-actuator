package com.spring.cloud.actuator.quartz.model

import java.util.Date

data class TriggerModel(
    val group: String,
    val name: String,
    val state: String,
    val nextFireTime: Date,
    val previousFireTime: Date,
    val startTime: Date,
    val endTime: Date,
)
