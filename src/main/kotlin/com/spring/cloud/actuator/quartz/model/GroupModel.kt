package com.spring.cloud.actuator.quartz.model

data class GroupModel<T>(private val groups: MutableMap<String, MutableList<T>> = HashMap()) {

    fun add(group: String, model: T) {
        groups.computeIfAbsent(group) { ArrayList() }.add(model)
    }
}
