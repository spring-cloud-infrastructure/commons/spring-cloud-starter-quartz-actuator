package com.spring.cloud.actuator.quartz.endpoint

import com.spring.cloud.actuator.quartz.exception.UnsupportedStateChangeException
import com.spring.cloud.actuator.quartz.model.GroupModel
import com.spring.cloud.actuator.quartz.model.QuartzState
import com.spring.cloud.actuator.quartz.model.QuartzState.PAUSE
import com.spring.cloud.actuator.quartz.model.QuartzState.RESUME
import com.spring.cloud.actuator.quartz.model.TriggerDetailModel
import com.spring.cloud.actuator.quartz.model.TriggerModelBuilder
import org.quartz.Scheduler
import org.quartz.TriggerKey
import org.quartz.impl.matchers.GroupMatcher.anyTriggerGroup
import org.quartz.impl.matchers.GroupMatcher.triggerGroupEquals
import org.springframework.boot.actuate.endpoint.annotation.Endpoint
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation
import org.springframework.boot.actuate.endpoint.annotation.Selector
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation

@Endpoint(id = "quartz-triggers")
class QuartzTriggerEndPoint(private val scheduler: Scheduler) {

    @ReadOperation
    fun listTriggers(
        group: String?,
        name: String?,
    ): GroupModel<TriggerDetailModel> {
        val triggerGroupMatcher = when (group) {
            null -> anyTriggerGroup()
            else -> triggerGroupEquals(group)
        }
        var triggerKeys = scheduler.getTriggerKeys(triggerGroupMatcher)
        if (name != null) {
            triggerKeys = triggerKeys.filter { name == it.name }.toMutableSet()
        }

        return GroupModel<TriggerDetailModel>().apply {
            triggerKeys.forEach { add(it.group, createTriggerModel(it)) }
        }
    }

    @WriteOperation
    fun modifyTriggerState(
        @Selector group: String,
        @Selector name: String,
        @Selector state: QuartzState,
    ): Boolean {
        val triggerKey = TriggerKey(name, group)
        when {
            !scheduler.checkExists(triggerKey) -> return false
            state === PAUSE -> scheduler.pauseTrigger(triggerKey)
            state === RESUME -> scheduler.resumeTrigger(triggerKey)
            else -> throw UnsupportedStateChangeException("Unsupported state change to: [$state]")
        }
        return true
    }

    @WriteOperation
    fun modifyTriggersState(
        @Selector group: String,
        @Selector state: QuartzState,
    ): Boolean {
        val triggerGroupMatcher = triggerGroupEquals(group)
        when {
            scheduler.getTriggerKeys(triggerGroupMatcher).isEmpty() -> return false
            state === PAUSE -> scheduler.pauseTriggers(triggerGroupMatcher)
            state === RESUME -> scheduler.resumeTriggers(triggerGroupMatcher)
            else -> throw UnsupportedStateChangeException("unsupported state change to state: [$state]")
        }
        return true
    }

    private fun createTriggerModel(triggerKey: TriggerKey): TriggerDetailModel {
        val trigger = scheduler.getTrigger(triggerKey)
        return TriggerModelBuilder().buildTriggerDetailModel(scheduler, trigger)
    }
}
