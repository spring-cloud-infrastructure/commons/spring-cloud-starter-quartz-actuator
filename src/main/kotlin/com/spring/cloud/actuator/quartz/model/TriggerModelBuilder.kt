package com.spring.cloud.actuator.quartz.model

import mu.KotlinLogging.logger
import org.quartz.JobKey
import org.quartz.Scheduler
import org.quartz.SchedulerException
import org.quartz.Trigger
import org.quartz.TriggerKey

class TriggerModelBuilder {

    private val logger = logger { }

    fun buildTriggerDetailModel(
        scheduler: Scheduler,
        jobKey: JobKey,
    ): List<TriggerDetailModel> = scheduler.getTriggersOfJob(jobKey).map { buildTriggerDetailModel(scheduler, it) }

    fun buildTriggerDetailModel(
        scheduler: Scheduler,
        triggerKey: TriggerKey,
    ): TriggerDetailModel {
        val trigger = scheduler.getTrigger(triggerKey)
        return buildTriggerDetailModel(scheduler, trigger)
    }

    fun buildTriggerDetailModel(
        scheduler: Scheduler,
        trigger: Trigger,
    ): TriggerDetailModel {
        val jobKey: String = try {
            scheduler.getTriggerState(trigger.key).toString()
        } catch (e: SchedulerException) {
            logger.warn(e) { "Cannot extract job key from trigger: [${trigger.key}]" }
            "Could not set due to error"
        }

        return TriggerDetailModel(
            trigger.key.name,
            trigger.nextFireTime,
            trigger.previousFireTime,
            trigger.startTime,
            trigger.endTime,
            trigger.key.group,
            trigger.jobKey.toString(),
            jobKey
        )
    }
}
