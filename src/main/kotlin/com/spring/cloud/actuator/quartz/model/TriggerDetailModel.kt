package com.spring.cloud.actuator.quartz.model

import java.util.Date

class TriggerDetailModel(
    val name: String,
    val nextFireTime: Date,
    val previousFireTime: Date,
    val startTime: Date,
    val endTime: Date,
    val group: String,
    val state: String,
    val jobKey: String,
)
