package com.spring.cloud.actuator.quartz.model

open class JobModel(
    val name: String,
    val isConcurrentDisallowed: Boolean,
    val isDurable: Boolean,
    val jobClass: String,
)
