package com.spring.cloud.actuator.quartz.autoconfigure

import com.spring.cloud.actuator.quartz.endpoint.QuartzTriggerEndPoint
import com.spring.cloud.actuator.quartz.endpoint.QuartzTriggerEndPointWebExtension
import org.quartz.Scheduler
import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnAvailableEndpoint
import org.springframework.boot.autoconfigure.AutoConfigureAfter
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
@ConditionalOnAvailableEndpoint(endpoint = QuartzTriggerEndPoint::class)
@AutoConfigureAfter(QuartzAutoConfiguration::class)
class QuartzTriggerEndPointAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    fun quartzTriggerEndPoint(scheduler: Scheduler): QuartzTriggerEndPoint = QuartzTriggerEndPoint(scheduler)

    @Bean
    @ConditionalOnBean(QuartzTriggerEndPoint::class)
    fun quartzTriggerEndPointWebExtension(quartzTriggerEndPoint: QuartzTriggerEndPoint): QuartzTriggerEndPointWebExtension =
        QuartzTriggerEndPointWebExtension(quartzTriggerEndPoint)
}
