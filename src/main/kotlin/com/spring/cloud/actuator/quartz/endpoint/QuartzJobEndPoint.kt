package com.spring.cloud.actuator.quartz.endpoint

import com.spring.cloud.actuator.quartz.exception.UnsupportedStateChangeException
import com.spring.cloud.actuator.quartz.model.GroupModel
import com.spring.cloud.actuator.quartz.model.JobDetailModel
import com.spring.cloud.actuator.quartz.model.JobModel
import com.spring.cloud.actuator.quartz.model.QuartzState
import com.spring.cloud.actuator.quartz.model.QuartzState.PAUSE
import com.spring.cloud.actuator.quartz.model.QuartzState.RESUME
import com.spring.cloud.actuator.quartz.model.TriggerModelBuilder
import org.quartz.JobKey
import org.quartz.Scheduler
import org.quartz.impl.matchers.GroupMatcher.anyJobGroup
import org.quartz.impl.matchers.GroupMatcher.jobGroupEquals
import org.springframework.boot.actuate.endpoint.annotation.Endpoint
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation
import org.springframework.boot.actuate.endpoint.annotation.Selector
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation

@Endpoint(id = "quartz-jobs")
class QuartzJobEndPoint(private val scheduler: Scheduler) {

    @ReadOperation
    fun listJobs(
        group: String?,
        name: String?,
    ): GroupModel<JobModel> {
        val jobGroupMatcher = when (group) {
            null -> anyJobGroup()
            else -> jobGroupEquals(group)
        }
        var jobKeys = scheduler.getJobKeys(jobGroupMatcher)
        if (name != null) {
            jobKeys = jobKeys.filter { name == it.name }.toMutableSet()
        }

        return GroupModel<JobModel>().apply {
            jobKeys.forEach { add(it.group, createJobModel(it)) }
        }
    }

    @ReadOperation
    fun getJobDetail(
        @Selector group: String,
        @Selector name: String,
    ): JobModel? {
        val jobKey = JobKey(name, group)
        return when {
            scheduler.checkExists(jobKey) -> createJobModel(jobKey)
            else -> null
        }
    }

    @WriteOperation
    fun modifyJobState(
        @Selector group: String,
        @Selector name: String,
        @Selector state: QuartzState,
    ): Boolean {
        val jobKey = JobKey(name, group)
        when {
            !scheduler.checkExists(jobKey) -> return false
            state === PAUSE -> scheduler.pauseJob(jobKey)
            state === RESUME -> scheduler.resumeJob(jobKey)
            else -> throw UnsupportedStateChangeException("Unsupported state change to: [$state]")
        }
        return true
    }

    @WriteOperation
    fun modifyJobsState(
        @Selector group: String,
        @Selector state: QuartzState,
    ): Boolean {
        val jobGroupMatcher = jobGroupEquals(group)
        when {
            scheduler.getJobKeys(jobGroupMatcher).isEmpty() -> return false
            state === PAUSE -> scheduler.pauseJobs(jobGroupMatcher)
            state === RESUME -> scheduler.resumeJobs(jobGroupMatcher)
            else -> throw UnsupportedStateChangeException("unsupported state change to state: [$state]")
        }
        return true
    }

    private fun createJobModel(key: JobKey): JobModel {
        val jobDetail = scheduler.getJobDetail(key)
        val jobTriggers = TriggerModelBuilder().buildTriggerDetailModel(scheduler, jobDetail.key)
        return JobDetailModel(
            name = jobDetail.key.name,
            isConcurrentDisallowed = jobDetail.isDurable,
            isDurable = jobDetail.isConcurrentExectionDisallowed,
            jobClass = jobDetail.jobClass.name,
            group = jobDetail.key.group,
            triggers = jobTriggers
        )
    }
}
