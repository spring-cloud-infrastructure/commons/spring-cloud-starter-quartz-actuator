package com.spring.cloud.actuator.quartz.autoconfigure

import com.spring.cloud.actuator.quartz.endpoint.QuartzJobEndPoint
import com.spring.cloud.actuator.quartz.endpoint.QuartzJobEndPointWebExtension
import org.quartz.Scheduler
import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnAvailableEndpoint
import org.springframework.boot.autoconfigure.AutoConfigureAfter
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
@ConditionalOnAvailableEndpoint(endpoint = QuartzJobEndPoint::class)
@AutoConfigureAfter(QuartzAutoConfiguration::class)
class QuartzJobEndPointAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    fun quartzJobEndPoint(scheduler: Scheduler): QuartzJobEndPoint = QuartzJobEndPoint(scheduler)

    @Bean
    @ConditionalOnBean(QuartzJobEndPoint::class)
    fun quartzJobEndPointWebExtension(quartzJobEndPoint: QuartzJobEndPoint): QuartzJobEndPointWebExtension =
        QuartzJobEndPointWebExtension(quartzJobEndPoint)
}
